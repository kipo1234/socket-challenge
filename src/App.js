import React, { Component } from 'react';
import io from 'socket.io-client';

class App extends Component {

  state = {
    isConnected:false,
    id:null,
    codi:[],
    answer: ""
  }
  
  socket = null

  componentDidMount(){

    this.socket = io('http://codicoda.com:8000');

    this.socket.on('connect', () =>{
      this.setState({isConnected:true})
    })
    this.socket.on('pong!',(additionalStuff)=>{
      console.log('the server answered!', additionalStuff)
    })
     this.socket.on('youare',(answer)=> {
       this.setState({id:answer.id})
     })

     this.socket.on('youare', (answer) => {
       this.setState({id:answer.id})
     })
     
     this.socket.on('room', old_messages => console.log(old_messages))

     this.socket.on('next', (message_from_server)=> console.log(message_from_server))
    
     this.socket.on('peeps', (codi)=>this.setState({codi}))
     this.socket.on('disconnect', () => {
      this.setState({isConnected:false})
    })



  }

  componentWillUnmount(){
    this.socket.close()
    this.socket = null
  }

  render() {
    return (
      <div className="App">
        <div>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
        <div>id: {this.state.id}</div>
      <button onClick={()=>this.socket.emit('whoami')}>Who am I?</button>
        <hr/>
        
        <input type="text" name="answer" onChange={e => this.setState({ answer: e.target.value })} />
        <button onClick={()=>this.socket.emit('answer',this.state.answer)}>submit answer</button>
        <ul>
          <p>{this.state.codi.length}  people are connected</p>
          {this.state.codi.map(friend=><li>{friend}</li>)}
        </ul>

      </div>

    );
  }

}

export default App;
